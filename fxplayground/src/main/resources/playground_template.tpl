{
    name: "${name}",
    description: "${description}",
    useFXML: ${useFXML},
    detachOutput: ${detachOutput},
    resources: ${resources},
    markupLanguage: "${markupLanguage}",
    codeLanguage: "${codeLanguage}",
    markupTextFilename: "${markupTextFilename}",
    codeTextFilename: "${codeTextFilename}",
    cssTextFilename: "${cssTextFilename}"
}