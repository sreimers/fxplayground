/*
* Copyright 2014 FXPlayground.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.fxplayground.services;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import org.fxplayground.beans.CompiledResult;
import org.fxplayground.beans.CompilerException;
import org.fxplayground.beans.Language;
import scala.collection.immutable.$colon$colon;
import scala.collection.immutable.$colon$colon$;
import scala.collection.immutable.Nil$;
import scala.tools.nsc.interpreter.IMain;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
* User: cdea
* Date: 6/30/2014
*/
public class DefaultFXPGCodeManagerImpl implements FXPGCodeManager {
    private static final Logger logger = Logger.getLogger(DefaultFXPGCodeManagerImpl.class.getName());
    private static Map<Language, ClassLoader> previousClassLoaderMap = new HashMap<>( );
    private static Map<Language, ScriptEngineManager> scriptEngineManagerHashMap = new HashMap<>();
    private static List<Language> languages = new ArrayList<>();

    static {
        Language nashorn = createLanguage("JavaScript (Nashorn)", "nashorn", true, "javascript", "js");
        scriptEngineManagerHashMap.put(nashorn, null);
        languages.add(nashorn);

        Language javascript = createLanguage("JavaScript (Webkit)", "webkit", true, "javascript", "js");
        //scriptEngineManagerHashMap.put(javascript, null);
        languages.add(javascript);

        Language groovy = createLanguage("Groovy 2.3.6", "groovy", true, "groovy", "groovy");
        scriptEngineManagerHashMap.put(groovy, null);
        languages.add(groovy);

        Language jruby = createLanguage("JRuby 1.7.13", "jruby", true, "ruby", "rb");
        scriptEngineManagerHashMap.put(jruby, null);
        languages.add(jruby);

        Language jython = createLanguage("Jython 2.7-b2", "jython", true, "python", "py");
        scriptEngineManagerHashMap.put(jython, null);
        languages.add(jython);

        Language clojure = createLanguage("Clojure 1.6.0", "Clojure", true, "clojure", "clj");
        scriptEngineManagerHashMap.put(clojure, null);
        languages.add(clojure);

        Language scala = createLanguage("Scala 2.11.2", "scala", true, "scala", "scala");
        scriptEngineManagerHashMap.put(scala, null);
        languages.add(scala);

//        Language rLang = createLanguage("R Renjin 0.7.0-RC7", "Renjin", true, "r", "r");
//        scriptEngineManagerHashMap.put(rLang, null);
//        languages.add(rLang);
    }
    private static Language createLanguage(String name, String engineName, boolean dynamic, String editorInfo, String fileExtension) {
        Language language = new Language();
        language.setName(name);
        language.setScriptEngineName(engineName);
        language.setDynamic(dynamic);
        language.setEditorInfo(editorInfo);
        language.setFileExtension(fileExtension);
        return language;
    }

    @Override
    public Language findByName(final String engineName) {

//        Language lang = scriptEngineManagerHashMap
//                .keySet()
//                .stream()
//                .filter(pred -> engineName.equalsIgnoreCase(pred.getScriptEngineName()))
//                .findFirst()
//                .get();
        Language lang = null;
        for(int i=0; i<languages.size(); i++) {
            Language tmplang = languages.get(i);
            logger.log(Level.FINE, "Engine name = " + tmplang.getScriptEngineName());
            if (engineName.equalsIgnoreCase(tmplang.getScriptEngineName())) {
                lang = tmplang;
                break;
            }
        }
        logger.log(Level.FINE, "Found language: " + lang.getScriptEngineName());
        return lang;
        //return groovy;
    }

    public void resetEngine(Language language) {
        ScriptEngineManager engine = scriptEngineManagerHashMap.get(language);
        if (engine != null) {
            engine.getBindings().clear();
        }
        scriptEngineManagerHashMap.put(language, null);
    }
    @Override
    public java.util.List<Language> getSupportedLanguages() {
        return languages;
    }

    @Override
    public CompiledResult compile(Language language,
                                  String sourceCode,
                                  Map<String, Object> bindings,
                                  String displaySurfaceVariableName,
                                  String displaySurfaceNodeId,
                                  ClassLoader classLoader) {

        CompiledResult compiledResult = new CompiledResult();
        if (language == null) {
            language = findByName("nashorn");
        }
        ScriptEngineManager scriptEngineManager = scriptEngineManagerHashMap.get(language);
        ScriptEngine engine = null;
                // if null or a different class loader than before recreate script manager
        ClassLoader previousClassLoader = previousClassLoaderMap.get(language);
        if (null == previousClassLoader || classLoader != previousClassLoader) {
            previousClassLoaderMap.put(language, classLoader);
            // TODO: clear previous scriptEngineManager HOW?
            scriptEngineManager = new ScriptEngineManager(classLoader);
            // create a script engine
            scriptEngineManagerHashMap.put(language, scriptEngineManager);
        }
        logger.log(Level.FINE,"Engine name: " + language.getScriptEngineName());

        engine = scriptEngineManager.getEngineByName(language.getScriptEngineName());
        //scriptEngineManager = new NashornScriptEngineFactory().getScriptEngine(classLoader);
//  https://issues.scala-lang.org/browse/SI-7916 Scala support is put on hold till the following is implemented.
//   returnResult = engine.getBindings(ScriptContext.ENGINE_SCOPE).get("someVar");

        if ("scala".equals(language.getScriptEngineName())) {
            // Set up Scriptenvironment to use the Java classpath
            scala.collection.immutable.List nil = Nil$.MODULE$;
            $colon$colon vals = $colon$colon$.MODULE$.apply((String) "true", nil);
            ((IMain)engine).settings().usejavacp().tryToSet(vals); //ScriptContext.ENGINE_SCOPE;
            sourceCode = sourceCode + "\nDISPLAY_SURFACE\n";
        }
        Object returnResult = null;
        try {
            logger.log(Level.FINE, "Script engine name: " + engine);
            engine.getContext().getBindings(ScriptContext.ENGINE_SCOPE).putAll(bindings);
            returnResult = engine.eval(String.valueOf(sourceCode));
            logger.log(Level.FINE, "Results returned from caller: " + returnResult);
        } catch (ScriptException e) {
            compiledResult.setCompilerException(new CompilerException(e));
        }
        if (returnResult != null && returnResult instanceof Pane) {
            // Ruby works if the last statment is the DISPLAY_SURFACE (implicit return)
            logger.log(Level.FINE, "Returned as " + returnResult);
        } else {
            if (displaySurfaceVariableName != null) {
                returnResult = engine.getBindings(ScriptContext.ENGINE_SCOPE).get(displaySurfaceVariableName);

            }
        }
        if (returnResult != null && returnResult instanceof Node) {
            ((Node) returnResult).setId(displaySurfaceNodeId);
        }
        logger.log(Level.FINE, "Resulting returned DISPLAY_SURFACE object: " + returnResult);
        compiledResult.setResult(returnResult);
        return compiledResult;
    }
}
