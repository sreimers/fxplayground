/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.utils;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: cdea
 * Date: 7/22/2014
 */
public class TemplateUtil {
    private TemplateUtil(){
    }

    /**
     * Given a template and a map of bindings transform a resultant text. Use a ${named_param} as parameters to be
     * substituted.
     * @param template A string template containing ${named_param}
     * @param bindings A map containing the name value pairs to be substituted.
     * @return String of the resultant text.
     */
    public static String transform(String template, Map<String, Object> bindings) {
        Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}");
        Matcher m = pattern.matcher(template);
        String result = template;
        while (m.find()) {
            String s = m.group(1);
            Object val = bindings.get(s);
            String value = val != null ? String.valueOf(val) : null;
            if (value != null) {
                result = result.replaceAll("\\$\\{" + s + "\\}", value);
            }
        }
        return result;
    }
}
