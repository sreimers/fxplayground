/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.logging.Logger;

/**  FXPlayground will use various files as resources to be used in a project.
 * Some resources can be other CSS files, images, fxml, etc.
 *
 * User: cdea
 * Date: 6/30/2014
 */
public class FXPGResource {
    private static final Logger logger = Logger.getLogger(FXPGResource.class.getName());
    private StringProperty name = new SimpleStringProperty();
    private StringProperty version = new SimpleStringProperty();
    private StringProperty url = new SimpleStringProperty();
    public FXPGResource() {

    }
    public FXPGResource(String name, String version, String url) {
        setName(name);
        setVersion(version);
        setUrl(url);
    }
    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getVersion() {
        return version.get();
    }

    public StringProperty versionProperty() {
        return version;
    }

    public void setVersion(String version) {
        this.version.set(version);
    }
    public String toString() {
        return getName();
    }

}
