/*
 * Copyright 2014 FXPlayground.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fxplayground.beans;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.logging.Logger;

/**
 * User: cdea
 * Date: 6/30/2014
 */
public class Language {
    private static final Logger logger = Logger.getLogger(Language.class.getName());
    private StringProperty name = new SimpleStringProperty();
    private StringProperty scriptEngineName = new SimpleStringProperty();
    private BooleanProperty dynamic = new SimpleBooleanProperty(true);
    private StringProperty editorInfo = new SimpleStringProperty();
    private StringProperty fileExtension = new SimpleStringProperty();

    public final String getName() {
        return name.get();
    }

    public final StringProperty nameProperty() {
        return name;
    }

    public final void setName(String name) {
        this.name.set(name);
    }

    public String getScriptEngineName() {
        return scriptEngineName.get();
    }

    public StringProperty scriptEngineNameProperty() {
        return scriptEngineName;
    }

    public void setScriptEngineName(String scriptEngineName) {
        this.scriptEngineName.set(scriptEngineName);
    }

    public boolean getDynamic() {
        return dynamic.get();
    }

    public BooleanProperty dynamicProperty() {
        return dynamic;
    }

    public void setDynamic(boolean dynamic) {
        this.dynamic.set(dynamic);
    }

    public String getEditorInfo() {
        return editorInfo.get();
    }

    public StringProperty editorInfoProperty() {
        return editorInfo;
    }

    public void setEditorInfo(String editorInfo) {
        this.editorInfo.set(editorInfo);
    }

    public String getFileExtension() {
        return fileExtension.get();
    }

    public StringProperty fileExtensionProperty() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension.set(fileExtension);
    }
}
