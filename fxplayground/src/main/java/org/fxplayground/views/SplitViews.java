package org.fxplayground.views;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Worker;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.SubScene;
import javafx.scene.control.SplitPane;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <pre>
 * +---+------+--------+
 * |   |      |        |
 * | A |  B   |   C    |
 * |   |      +--------+
 * |   +------|        |
 * |   |      |        |
 * |   |  D   |   E    |
 * |   |      |        |
 * +---+------+--------+
 *
 * A - vertical left is the project options.
 * B - fxml/html window
 * C - CSS window
 * D - language window
 * E - output window
 * </pre>
 * User: cdea
 * Date: 6/15/2014
 */
public class SplitViews extends BorderPane {
    private static Logger logger = Logger.getLogger(SplitViews.class.getName());
    private StringProperty currentEditorProperty = new SimpleStringProperty();
    // current editor's editor selected

    // B - fxml/html editor window
    WebView domEditor;

    // C - CSS editor window
    WebView cssEditor;

    // D - language editor window
    WebView languageEditor;

    // E - result window
    Pane outputPane = new Pane();
    SubScene outputSubScene;
    Pane outputSubSceneRoot = new Pane();

    SplitPane cssAndResultSplit = new SplitPane();

    public SplitViews() {
        URL url = null;
        String urlString = null;
        try {
            url = new File(System.getProperty("user.dir")).toURI().toURL();
            urlString = url.toURI().toURL().toString();
        } catch (MalformedURLException e) {
            logger.log(Level.SEVERE,"Unable to obtain user's working directory.", e);
        } catch (URISyntaxException e) {
            logger.log(Level.SEVERE,"Unable to obtain user's working directory.", e);
        }
        logger.log(Level.FINE, urlString + "ace-editor/dom-editor.html");
        domEditor =  loadEditor(urlString + "ace-editor/dom-editor.html");
        cssEditor =  loadEditor(urlString + "ace-editor/css-editor.html");
        languageEditor = loadEditor(urlString + "ace-editor/language-editor.html");
        //languageEditor = loadEditor(urlString + "ace-editor/groovy-editor.html");

        // build A | (B, C)
        //         |-------
        //         | (D, E)

        SplitPane sp = new SplitPane();
        sp.setOrientation(Orientation.HORIZONTAL);

        // DOM editor       [ B ]
        // language editor  [ D ]
        SplitPane domAndLangSplit = new SplitPane();
        domAndLangSplit.setOrientation(Orientation.VERTICAL);
        domAndLangSplit.getItems().addAll(domEditor, languageEditor);

        // CSS editor       [ C ]
        // result pane      [ E ]
        cssAndResultSplit.setOrientation(Orientation.VERTICAL);

        // Create a the result pane to hold a sub scene node.
        outputPane = new Pane();

        wireOutput(outputSubSceneRoot);
//        // Create a SubScene to hold the user's root pane.
//        outputSubScene = new SubScene(outputSubSceneRoot, 300, 400);
//        outputPane.getChildren().add(outputSubScene);
//
//        // Make scene dimension's bound to the split area.
//        outputSubScene.setId("display-sub-scene");
//        outputSubScene.widthProperty().bind(outputPane.widthProperty());
//        outputSubScene.heightProperty().bind(outputPane.heightProperty());
        cssAndResultSplit.getItems().addAll(cssEditor, outputPane);

        // DOM editor and CSS editor  [ B | C ]
        // language editor and result [ D | E ]
        SplitPane domCss_langResultSplit = new SplitPane();
        domCss_langResultSplit.setOrientation(Orientation.HORIZONTAL);
        domCss_langResultSplit.getItems().addAll(domAndLangSplit, cssAndResultSplit);

        sp.getItems().add(domCss_langResultSplit);
        //sp.setDividerPositions(0.3f, 0.6f, 0.9f);
        setCenter(sp);
    }

    public WebView getDomEditor() {
        return domEditor;
    }

    public void setDomEditor(WebView domEditor) {
        this.domEditor = domEditor;
    }

    public WebView getCssEditor() {
        return cssEditor;
    }

    public void setCssEditor(WebView cssEditor) {
        this.cssEditor = cssEditor;
    }

    public WebView getLanguageEditor() {
        return languageEditor;
    }

    public void setLanguageEditor(WebView languageEditor) {
        this.languageEditor = languageEditor;
    }

    public Pane getOutputSubSceneRoot() {
        return outputSubSceneRoot;
    }

    /**
     * @todo there is a confusion between outputSubSceneRoot and user defined display surface.
     * @param outputSubSceneRoot
     */
    public void setOutputSubSceneRoot(Pane outputSubSceneRoot) {
        if (this.outputSubSceneRoot != null) {
            this.outputSubSceneRoot.getChildren().clear();
            this.outputSubSceneRoot.setId("SubSceneRoot");
            logger.log(Level.FINE, "this.outputSubSceneRoot = " + this.outputSubSceneRoot.getId() + " " + this.outputSubSceneRoot);
            logger.log(Level.FINE, "outputSubSceneRoot = " + outputSubSceneRoot.getId() + " " + outputSubSceneRoot);
            this.outputSubSceneRoot.getChildren().add(outputSubSceneRoot);
        }
    }

    public void wireOutput(Pane userDisplay) {
        // Create a the result pane to hold a sub scene node.
        //outputPane = new Pane();

        // Create a SubScene to hold the user's root pane.
        //Pane testPane = new Pane();
        //testPane.getChildren().addAll(userDisplay.getChildren());
        //testPane.setId("display-surface");
        //outputSubScene = new SubScene(testPane, 300, 400);
        logger.log(Level.FINE, "userDisplay's parents" + userDisplay.getParent());
        logger.log(Level.FINE, String.valueOf(userDisplay));
        userDisplay.setId("display-surface");
        if (outputSubScene == null) {
            outputSubScene = new SubScene(userDisplay, 300, 400);
        }
        //outputSubScene = new SubScene(userDisplay, 300, 400);
        outputSubScene.setRoot(userDisplay);
        outputPane.getChildren().remove(outputSubScene);
        outputPane.getChildren().add(outputSubScene);
        // Make scene dimension's bound to the split area.
        outputSubScene.setId("display-sub-scene");
        outputSubScene.widthProperty().bind(outputPane.widthProperty());
        outputSubScene.heightProperty().bind(outputPane.heightProperty());
    }
    public void unwireOutput() {
        // Create a the result pane to hold a sub scene node.
        outputPane.getChildren().clear();
        outputSubScene.widthProperty().unbind();
        outputSubScene.heightProperty().unbind();
        //outputSubScene.setRoot(null);
    }
    private AnchorPane loadProjectOptions() {
        try {
            return FXMLLoader.load(getClass().getResource("/fxplayground_options.fxml"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to load project's settings option view (fxplayground_options.fxml)", e);
        }
        return null;
    }


    private WebView loadEditor(String url) {
        WebView webView = new WebView();
        webView.getEngine().setJavaScriptEnabled(true);
        //enableFirebug(webView);

        // The web view's web engine
        webView.getEngine()
                .getLoadWorker()
                .stateProperty()
                .addListener( (obs, oldValue, newValue) -> {
                    if (newValue == Worker.State.SUCCEEDED) {
                        logger.log(Level.FINE, "Finished loading %s \n", url);
                        JSObject jsobj = (JSObject) webView.getEngine().executeScript("window");
                        jsobj.setMember("$MAIN", this);

                    }
                });

        webView.getEngine().setOnAlert((WebEvent<String> t) -> {
            logger.log(Level.FINE, "Alert Event - Message: " + t.getData());
            if(t.getData()!=null && !t.getData().equals("") && t.getData().startsWith("copy: ")){
                doCopy(t.getData().substring(6));
            }
        });

        final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_DOWN);
        webView.addEventHandler(KeyEvent.KEY_RELEASED, keyEvent ->  {
            if (keyComb1.match(keyEvent)) {
                logger.log(Level.FINE, "Ctrl+V pressed");
                doPaste(webView.getEngine());
            }
        });


        webView.getEngine().load(url);
        return webView;
    }
    public final void doCopy(String txt) {
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        final ClipboardContent content = new ClipboardContent();
        content.putString(txt);
        clipboard.setContent(content);
    }
    // bridge to paste from external clipboard into the editor
    public final void doPaste(WebEngine webEngine) {
        final Clipboard clipboard = Clipboard.getSystemClipboard();
        String content = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);

//        content = content.replaceAll("\\\\", "\\\\\\\\");
//        content = content.replaceAll("\"","\\\\\"");
//        content = content.replaceAll("\\n","\\\\n");
//        content = content.replaceAll("\\r","\\\\r");
        webEngine.executeScript("( pasteContent(" + getLiteral(content) + ") )");
    }
    private Pane loadResultPane() {
        Pane pane = new Pane();

        return pane;
    }
    public void println(String s) {
        System.out.println(s);
    }

    public static String getLiteral(String code){
        StringProperty lit = new SimpleStringProperty("\"");
        code.chars().forEach(v->lit.set(lit.get().concat(char2Literal((char)v))));
        return lit.get().concat("\"");
    }
    private static String char2Literal(char val) {
        if (val == 10) return "\\n";
        else if (val == 13) return "\\r";
        else if (val == 92) return "\\\\";
        else if (val == 34) return "\\\"";
        else return String.valueOf(val);
    }
    public StringProperty currentEditorProperty(){
        return currentEditorProperty;
    }
    public final void setCurrentEditorProperty(String editor) {
       currentEditorProperty.set(editor);
    }
    public final String getCurrentEditorProperty() {
        return currentEditorProperty.get();
    }

    public SubScene getOutputSubScene() {
        return outputSubScene;
    }

    public void setOutputSubScene(SubScene outputSubScene) {
        this.outputSubScene = outputSubScene;
    }

}
