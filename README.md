# README #

Welcome to FX Playground
This project allows a JavaFX or Html5 developer to run code on the fly without needing to compile a Java project.

After cloning the project navigate to the subdirectory fxplayground to perform the gradle build. To build and run the project you should be the same directory where the build.gradle file is. 

You can build the project by calling 


```

gradle build
```

### Run the application ###

```

gradle run
```
### What is this repository for? ###

* An application developer wanting to rapidly develop and execute code without recompiling. Allows the developer to prototype HTML5, JavaFX, ruby, groovy, python, scala, clojure, and JavaScript.
* Version 1.0



### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact